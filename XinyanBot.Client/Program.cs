using ChipBot.Core.Web.Extensions;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using System.Reflection;
using XinyanBot.Client;
using XinyanBot.Client.Services;
using XinyanBot.Client.Shared;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddSingleton<ThemeManager>();

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

builder.Services.AddChipBotWebCore(new List<Assembly>() {
    Assembly.GetExecutingAssembly(),
}, typeof(AppLayout));

await builder.Build().RunAsync();
