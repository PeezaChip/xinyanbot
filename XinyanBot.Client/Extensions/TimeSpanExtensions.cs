﻿namespace XinyanBot.Client.Extensions
{
    public static class TimeSpanExtensions
    {
        public static string ToTrackTime(this TimeSpan ts)
            => $"{Math.Floor(ts.TotalMinutes):0}:{ts.Seconds:00}";
    }
}
