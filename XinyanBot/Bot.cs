﻿using ChipBot.Core.Abstract.Interfaces;

namespace XinyanBot
{
    public class Bot : IBot
    {
        public long CreatedTS => 1667396709000;

        public string Description => "is a private-use music Discord bot.";

        public string Thumbnail => "https://i.imgur.com/qSP3Mnc.png";
    }
}
