﻿using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Extensions;
using System.Reflection;
using XinyanBot;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IBot>(new Bot());
builder.AddChipCore(new List<Assembly>() {
    Assembly.GetExecutingAssembly(),
    Assembly.GetAssembly(typeof(XinyanPlugin.Plugin)),
});

var app = builder.Build();

app.UseChipCore();

app.Run();
