#!/bin/bash

echo "cache-bust"

files=("css/app.css")
dir="./bin/Release/net8.0/publish/wwwroot/"

for file in "${files[@]}"
do
    filePath="$dir$file"
    echo "proccessing $file"

    if [ ! -f $filePath ]; then
        echo "file not found $file"
        continue
    fi

    md5=$(md5sum "$filePath" | cut -d ' ' -f 1)
    echo "$file md5: $md5"

    newFile=${file/"."/".$md5."}
    newFilePath=$dir$newFile

    echo "$filePath > $newFilePath"

    mv $filePath $newFilePath

    sed -i "s~$file~$newFile~" $dir"index.html"
done